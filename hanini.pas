unit hanini;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, Dialogs, {HRadio, HGroupBx, HLabel, } Grids,
  Outline, DirOutln ,inifiles, FileCtrl {HRadGrup,} ;

type
  TFrm_hanini = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    Bevel1: TBevel;
    Mikum_han: TEdit;
    Mis_pkoda: TEdit;
    Label1: TLabel;
    Mis_Maam: TEdit;
    Spb_buttonMikum: TSpeedButton;
    Label3: TLabel;
    Label4: TLabel;
    Mavar_1: TEdit;
    Label5: TLabel;
    Mavar_2: TEdit;
    Label6: TLabel;
    Mavar_3: TEdit;
    Label7: TLabel;
    Label2: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure Spb_buttonMikumClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Frm_hanini: TFrm_hanini;
    P :Pchar ;
    HanStr,mikum,MisMaam,MisPkoda          : shortstring;
    MisMaam1,MisPkoda1                     : shortstring;
    MisMaam2,MisPkoda2                     : shortstring;
    MisMaam3,MisPkoda3                     : shortstring;
    Mavar1,Mavar2,Mavar3                   : shortstring;
    DirectoryIniFiles                      : shortstring;
    Hanprm,Nzrprm                          : Tinifile;
    PerutMaam,Asmachta2,HesbonNegdi,
    Tnuatzikuy,RikuzMiun,PratimLeHan,
    PerutMasKab                            : integer;
    MisHevra,MisHevra1,MisHevra2,MisHevra3 :shortstring ;
    AtmUserName,
    AtmPassword,
    DBAliasName   : String;

implementation

uses DirSelct;

{$R *.DFM}

procedure TFrm_hanini.FormCreate(Sender: TObject);
begin
    Frm_DirSelect := nil;
    P:=StrAlloc(100);
    GetWindowsDirectory(p,100);    {Windows �����}
    HanStr:=Strpas(p);
    StrDispose(p);
//  ============
    Hanprm:=Tinifile.create(Hanstr+'\Atmhan.ini');
    Mikum:=Hanprm.readString('Han','Mikum',''); { ����� ���� ���"� }
    {Ini  ����� ���� }

    Mispkoda:=Hanprm.readString('Han','Mispkoda','0');
    MisMaam:=Hanprm.readString('Han','MisMaam','');
    MisHevra:=Hanprm.readString('Han','MisHevra','');
    MisHevra1:=Hanprm.readString('Han','MisHevra1','');
    PerutMaam:=Hanprm.readInteger('Han','PerutMaam',0);
    Tnuatzikuy:=Hanprm.readInteger('Han','Tnuatzikuy',0);
    RikuzMiun:=Hanprm.readInteger('Han','RikuzMiun',0);
    Asmachta2:=Hanprm.readInteger('Han','Asmachta2',0);
    HesbonNegdi:=Hanprm.readInteger('Han','HesbonNegdi',0);
    PratimLeHan:=Hanprm.readInteger('Han','PratimLeHan',0);
    PerutMasKab:=Hanprm.readInteger('Han','PerutMasKab',0);

    Mavar1:=Hanprm.readString('Han','Mavar1','');
    Mavar2:=Hanprm.readString('Han','Mavar2','');
    Mavar3:=Hanprm.readString('Han','Mavar3','');

    Mikum_han.text:=Mikum;
    Mis_Pkoda.text:=Mispkoda;
    Mis_Maam.text:=MisMaam;
    Mavar_1.text:=Mavar1;
    Mavar_2.text:=Mavar2;
    Mavar_3.text:=Mavar3;
    Hanprm.free;
end;

procedure TFrm_hanini.OKBtnClick(Sender: TObject);
begin
        Hanprm:=Tinifile.create(Hanstr+'\Atmhan.ini');

        Hanprm.WriteString('Han','Mikum',Mikum_han.text);
        Hanprm.WriteString('Han','MisPkoda',Mis_Pkoda.text);
        Hanprm.WriteString('Han','MisMaam',Mis_Maam.text);
        Hanprm.WriteString('Han','Mavar1',Mavar_1.text);
        Hanprm.WriteString('Han','Mavar2',Mavar_2.text);
        Hanprm.WriteString('Han','Mavar3',Mavar_3.text);
        Hanprm.free;
end;

Procedure TFrm_hanini.Spb_buttonMikumClick(Sender: TObject);
Var
  Slash :String[1];
begin
     if Frm_DirSelect = nil then
        Frm_DirSelect:=Tfrm_DirSelect.Create(Nil);
     If Frm_DirSelect.ShowModal = mrOk Then
     begin
       Slash := Copy (Frm_DirSelect.DirectoryListBox1.Directory,Length(Frm_DirSelect.DirectoryListBox1.Directory),1);
       if Slash = '\' then
          Mikum_han.Text:=Frm_DirSelect.DirectoryListBox1.Directory
       else
          Mikum_han.Text:=Frm_DirSelect.DirectoryListBox1.Directory+'\';
     end;
     Frm_DirSelect.Free;
     Frm_DirSelect := nil;
end;

end.
