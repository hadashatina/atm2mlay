unit HanDoh;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, QuickRpt, Qrctrls ,inifiles;

type
  TFrm_Handoch = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRBand2: TQRBand;
    QRShape1: TQRShape;
    QRBand5: TQRBand;
    QRLabel10: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel11: TQRLabel;
    QRSysData2: TQRSysData;
    QRLabel2: TQRLabel;
    QRShape2: TQRShape;
    QRLabel4: TQRLabel;
    QRShape4: TQRShape;
    QRLabel5: TQRLabel;
    QRShape5: TQRShape;
    QRLabel6: TQRLabel;
    QRShape6: TQRShape;
    QRLabel7: TQRLabel;
    QRLabel3: TQRLabel;
    QRShape3: TQRShape;
    QRBand4: TQRBand;
    Qlb_detail: TQRLabel;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    Qlb_Sum: TQRLabel;
    Qlb_Teuda: TQRLabel;
    Qlb_Pail: TQRLabel;
    QRShape11: TQRShape;
    Qlb_Nehag: TQRLabel;
    Qlb_Num: TQRLabel;
    QRShape12: TQRShape;
    QRBand3: TQRBand;
    Qlb_Total: TQRLabel;
    QRLabel8: TQRLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QuickRep1BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QuickRep1AfterPreview(Sender: TObject);
    procedure QuickRep1NeedData(Sender: TObject; var MoreData: Boolean);
    procedure Qlb_TotalPrint(sender: TObject; var Value: String);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
    Frm_HAndoch: TFrm_HanDoch;
    line                        : Shortstring;
    FileHan                     : TextFile;
    Total                       : Real;

implementation

uses Atm, Dm_Atm;

{$R *.DFM}

procedure TFrm_HanDoch.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//      Frm_Handoch.Free;
//      Frm_Handoch := Nil;
end;

procedure TFrm_HanDoch.QuickRep1BeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
     AssignFile(FileHan,'Doch');
     Reset (FileHan);
     Total:=0;
//   Readln (FileHan,Line);
end;

procedure TFrm_HanDoch.QuickRep1AfterPreview(Sender: TObject);
begin
   Try
     CloseFile (FileHan);
   Except
   end;
end;

Procedure TFrm_HanDoch.QuickRep1NeedData(Sender: TObject;
  var MoreData: Boolean);

Begin
  If Not eof (FileHan) then
  Begin
    Readln (FileHan,Line);
    Qlb_sum.Caption:=Copy(Line,50,9);
    Qlb_detail.Caption:=Copy(Line,5,30);
    Qlb_Teuda.Caption:=Copy(Line,43,7);
    Qlb_Pail.Caption:=Copy(Line,41,2);
    Qlb_Nehag.Caption:=Copy(Line,35,6);
    Qlb_Num.Caption:=Copy(Line,1,4);
    Total:=Total+StrToFloat(Qlb_sum.Caption);
    MoreData :=True;
  end;
end;

procedure TFrm_Handoch.Qlb_TotalPrint(sender: TObject; var Value: String);
begin
     Value:=FormatFloat('0.00',Total);
end;

end.
