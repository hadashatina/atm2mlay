unit Frm_Clnd;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, Spin, Grids, Calendar;

type
  TFrmCalendar = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    Bevel1: TBevel;
    Clndr: TCalendar;
    Spe_year: TSpinEdit;
    Spe_month: TSpinEdit;
    procedure Spe_yearChange(Sender: TObject);
    procedure Spe_monthChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmCalendar: TFrmCalendar;

implementation

{$R *.DFM}

procedure TFrmCalendar.Spe_yearChange(Sender: TObject);
begin
     clndr.year:=spe_year.value;
end;

procedure TFrmCalendar.Spe_monthChange(Sender: TObject);
begin
    if spe_month.value<1  then
    Begin
           spe_month.value:=12;
           spe_YEAR.value:=spe_YEAR.value-1;
    End;
    if spe_month.value>12 then
    Begin
           spe_month.value:=1;
           spe_YEAR.value:=spe_YEAR.value+1;
    End;
    clndr.Month:=spe_month.value;
    spe_yearChange(sender);

end;

procedure TFrmCalendar.FormShow(Sender: TObject);
var
  Present: TDateTime;
  Year, Month, Day : Word;
begin
  Present:= Now;
  DecodeDate(Present, Year, Month, Day);
  clndr.day:=(Day);

  clndr.month:=(month);
  clndr.year:= (Year);

  spe_month.value:=clndr.month;
  spe_Year.value:=clndr.year

end;

end.
