library Atm2han;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
//  ExceptionLog,
  ExceptionLog,
  SysUtils,
  Classes,
  Atm in 'atm.pas',
  Dm_Atm in 'Dm_Atm.pas' {Frm_Data: TDataModule},
  DirSelct in 'DirSelct.pas' {Frm_DirSelect},
  HanPrt in 'HanPrt.pas' {Frm_Hanprt},
  HanDoh in 'HanDoh.pas' {Frm_Handoch};

{R *.RES$}
  exports
       InitAtmHan,
       DoneAtmHan,
       ShowAtmHan;

begin
  Frm_Main:=nil;
end.
