unit HanPrt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, QuickRpt, Qrctrls ,inifiles;

type
  TFrm_Hanprt = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRBand2: TQRBand;
    QRShape1: TQRShape;
    QRBand4: TQRBand;
    Qlb_detail: TQRLabel;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRBand5: TQRBand;
    QRLabel10: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel11: TQRLabel;
    QRSysData2: TQRSysData;
    QRBand3: TQRBand;
    QRShape19: TQRShape;
    QRLabel2: TQRLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QuickRep1BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QuickRep1AfterPreview(Sender: TObject);
    procedure QuickRep1NeedData(Sender: TObject; var MoreData: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
    Frm_Hanprt: TFrm_Hanprt;
    line                             : Shortstring;
    FileHan                          : TextFile;

implementation

uses Atm, Dm_Atm;

{$R *.DFM}

procedure TFrm_Hanprt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//      Frm_HanPrt.Free;
//      Frm_HanPrt := Nil;
end;

procedure TFrm_Hanprt.QuickRep1BeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
     AssignFile(FileHan,'Err.Err');
     Reset (FileHan);
//   Readln (FileHan,Line);
end;

procedure TFrm_Hanprt.QuickRep1AfterPreview(Sender: TObject);
begin
   Try
     CloseFile (FileHan);
   Except
   end;
end;

procedure TFrm_Hanprt.QuickRep1NeedData(Sender: TObject;
  var MoreData: Boolean);

begin
  if Not eof (FileHan) then
  Begin
    Readln (FileHan,Line);
    Qlb_detail.Caption:=Line;    
    MoreData :=True;
  end;
end;

end.
