unit HanDoch;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, QuickRpt, Qrctrls ,inifiles;

type
  TFrm_Handoch = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRBand2: TQRBand;
    QRShape1: TQRShape;
    QRBand4: TQRBand;
    Qlb_detail: TQRLabel;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRBand5: TQRBand;
    QRLabel10: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel11: TQRLabel;
    QRSysData2: TQRSysData;
    QRBand3: TQRBand;
    QRShape19: TQRShape;
    QRLabel2: TQRLabel;
    QRShape2: TQRShape;
    QRLabel3: TQRLabel;
    QRShape3: TQRShape;
    QRLabel4: TQRLabel;
    QRShape4: TQRShape;
    QRLabel5: TQRLabel;
    QRShape5: TQRShape;
    QRLabel6: TQRLabel;
    QRShape6: TQRShape;
    QRLabel7: TQRLabel;
    Qlb_sum: TQRDBText;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QuickRep1BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QuickRep1AfterPreview(Sender: TObject);
    procedure QuickRep1NeedData(Sender: TObject; var MoreData: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
    Frm_HAndoch: TFrm_HanDoch;
    line                             : Shortstring;
    FileHan                          : TextFile;

implementation

uses Atm, Dm_Atm;

{$R *.DFM}

procedure TFrm_HanDoch.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//      Frm_Handoch.Free;
//      Frm_Handoch := Nil;
end;

procedure TFrm_HanDoch.QuickRep1BeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
     AssignFile(FileHan,'FileDoch');
     Reset (FileHan);
//   Readln (FileHan,Line);
end;

procedure TFrm_HanDoch.QuickRep1AfterPreview(Sender: TObject);
begin
   Try
     CloseFile (FileHan);
   Except
   end;
end;

procedure TFrm_HanDoch.QuickRep1NeedData(Sender: TObject;
  var MoreData: Boolean);

begin
  if Not eof (FileHan) then
  Begin
    Readln (FileHan,Line);
    Qlb_sum.Caption:=Copy(Line,49,9);
    Qlb_detail.Caption:=Copy(Line,5,30);
    MoreData :=True;
  end;
end;

end.
